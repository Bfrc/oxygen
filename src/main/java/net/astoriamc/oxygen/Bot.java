package net.astoriamc.oxygen;

import net.astoriamc.oxygen.discord.listeners.ReadyEvent;
import net.astoriamc.oxygen.infinityJda.InfinityInit;
import net.astoriamc.oxygen.infinityJda.commandHandeler;
import net.astoriamc.oxygen.utils.Cache;
import net.astoriamc.oxygen.utils.Refrances;
import net.astoriamc.oxygen.utils.Terminal;
import net.astoriamc.oxygen.utils.mySql;
import net.astoriamc.oxygen.utils.socket.Server;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.api.sharding.DefaultShardManagerBuilder;
import net.dv8tion.jda.api.sharding.ShardManager;
import net.dv8tion.jda.api.utils.cache.CacheFlag;

import javax.security.auth.login.LoginException;
import java.io.IOException;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.Executors;

public class Bot {
    private final HashMap<String, User> Users = new HashMap<>();
    private final Connection connection;
    private final Bot bot = this;
    public boolean initialized = false;
    public ShardManager api;
    private Server server;
    private Cache cache = new Cache();

    public Bot() throws SQLException {
        Random random = new Random();
        if (random.nextBoolean()) {
            System.out.print("\u001b[34m" +
                    " ██████╗ ██╗  ██╗██╗   ██╗ ██████╗ ███████╗███╗   ██╗\n" +
                    "██╔═══██╗╚██╗██╔╝╚██╗ ██╔╝██╔════╝ ██╔════╝████╗  ██║\n" +
                    "██║   ██║ ╚███╔╝  ╚████╔╝ ██║  ███╗█████╗  ██╔██╗ ██║\n" +
                    "██║   ██║ ██╔██╗   ╚██╔╝  ██║   ██║██╔══╝  ██║╚██╗██║\n" +
                    "╚██████╔╝██╔╝ ██╗   ██║   ╚██████╔╝███████╗██║ ╚████║\n" +
                    " ╚═════╝ ╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚══════╝╚═╝  ╚═══╝\n\u001b[0m");
        }else {
           System.out.print("\u001b[33;1m" +
                   "██╗███╗   ██╗███████╗██╗███╗   ██╗██╗████████╗███████╗    ██████╗ ██╗   ██╗ ██████╗██╗  ██╗███████╗\n" +
                   "██║████╗  ██║██╔════╝██║████╗  ██║██║╚══██╔══╝██╔════╝    ██╔══██╗██║   ██║██╔════╝██║ ██╔╝██╔════╝\n" +
                   "██║██╔██╗ ██║█████╗  ██║██╔██╗ ██║██║   ██║   █████╗      ██║  ██║██║   ██║██║     █████╔╝ ███████╗\n" +
                   "██║██║╚██╗██║██╔══╝  ██║██║╚██╗██║██║   ██║   ██╔══╝      ██║  ██║██║   ██║██║     ██╔═██╗ ╚════██║\n" +
                   "██║██║ ╚████║██║     ██║██║ ╚████║██║   ██║   ███████╗    ██████╔╝╚██████╔╝╚██████╗██║  ██╗███████║\n" +
                   "╚═╝╚═╝  ╚═══╝╚═╝     ╚═╝╚═╝  ╚═══╝╚═╝   ╚═╝   ╚══════╝    ╚═════╝  ╚═════╝  ╚═════╝╚═╝  ╚═╝╚══════╝\n" +
                   "\u001b[0m");
        }
        try {
            DefaultShardManagerBuilder builder = DefaultShardManagerBuilder.createDefault(Refrances.token, GatewayIntent.GUILD_MEMBERS, GatewayIntent.GUILD_BANS, GatewayIntent.GUILD_VOICE_STATES, GatewayIntent.GUILD_MESSAGES, GatewayIntent.GUILD_MESSAGE_REACTIONS, GatewayIntent.DIRECT_MESSAGES, GatewayIntent.DIRECT_MESSAGE_REACTIONS, GatewayIntent.GUILD_EMOJIS, GatewayIntent.GUILD_PRESENCES)
                    .setShardsTotal(1)
                    .setShards(0, 0)
                    .setEnableShutdownHook(false)
                    .setAutoReconnect(true)
                    .setStatus(OnlineStatus.DO_NOT_DISTURB)
                    .setActivity(Activity.playing("Starting..."))
                    .setBulkDeleteSplittingEnabled(false)
                    .setCallbackPool(Executors.newFixedThreadPool(4))
                    .enableCache(CacheFlag.EMOTE, CacheFlag.ACTIVITY);
            MessageAction.setDefaultMentions(EnumSet.of(Message.MentionType.EMOTE, Message.MentionType.CHANNEL));
            api = builder.build();

        } catch (LoginException e) {
            Terminal.displayFatal("Could not log in aborting lunch");
            System.exit(1);
        }
        cache.hmap.put("740222298627244033", "Lobby");
        api.addEventListener(new ReadyEvent(this));
        while (this.api.getShards().stream().allMatch(s -> (s.getStatus() != JDA.Status.CONNECTED))) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException interruptedException) {
            }
        }
        Terminal.displayInfo("Api instance: " + api);
        new InfinityInit(this);
        api.addEventListener(new commandHandeler(this));

        server = new Server(bot);
        (new Thread(() -> {
            try {
                this.server.start(Refrances.SocketPort);
            } catch (IOException e) {
                Terminal.displayFatal("Could not start socked. Disabling");
                System.exit(1);
            }
        })).start();

        mySql mySql = new mySql();
        connection = mySql.openConnection();
        mySql.iniSql(connection);

        Thread thread = new Thread(new CommandThread(api, bot));

        thread.start();
    }

    public Connection getConnection() {
        return connection;
    }

    //Getter adder to Users
    //TODO: Auto wipe user after x sec
    public User getUser(String id) {
        return Users.get(id);
    }

    public Cache getCache() {
        return cache;
    }

    public void addUser(String id, User user) {
        Users.put(id, user);
    }

}
