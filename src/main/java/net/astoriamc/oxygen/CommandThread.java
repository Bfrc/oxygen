package net.astoriamc.oxygen;

import net.astoriamc.oxygen.infinityJda.InfinityRegister;
import net.astoriamc.oxygen.utils.Refrances;
import net.astoriamc.oxygen.utils.Terminal;
import net.dv8tion.jda.api.sharding.ShardManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class CommandThread implements Runnable {
    private ShardManager api;
    private Bot bot;

    public CommandThread(ShardManager api, Bot bot) {
        this.api = api;
        this.bot = bot;
    }

    @Override
    public void run() {
        while (true) {
            BufferedReader reader =
                    new BufferedReader(new InputStreamReader(System.in));
            String name = null;
            try {
                name = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
            String[] formatedLine = name.split(" ");
            switch (formatedLine[0]) {
                case "info":
                    Terminal.print("Oxygen prototype");
                    Terminal.print("By Bfrc");
                    Terminal.displayError("Simple error");
                    Terminal.displayWarning("Simple Warning");
                    Terminal.displayInfo("SimpleInfo");
                    continue;
                case "syncAll":
                    Terminal.displayInfo("Syncing in progress");
                    Connection connection = bot.getConnection();
                    Statement statement = null;
                    try {
                        statement = connection.createStatement();
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    HashMap<Integer, String> hashMap = new HashMap();
                    ResultSet result = null;
                    try {
                        String sql = "SELECT * FROM PlayerDataMc";
                        result = statement.executeQuery(sql);
                        while (result.next()) {
                            hashMap.put(result.getInt("UUPH"), result.getString("PlayerName"));
                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    try {
                        String sql = "SELECT * FROM UUPH WHERE UUIDDC IS NOT NULL;";
                        result = statement.executeQuery(sql);
                        while (result.next()) {
                            String dcid = result.getString("UUIDDC");
                            String rang = result.getString("RANG");
                            int uuph = result.getInt("UUPH");
                            String mcname = hashMap.get(uuph);
                            api.getGuildById(Refrances.guild).getMemberById(dcid).modifyNickname("[" + rang + "] " + mcname);

                        }
                    } catch (SQLException throwables) {
                        throwables.printStackTrace();
                    }
                    continue;
                case "showModules":
                    bot.getCache().ActiveCommands.forEach((k, v) -> {
                        Terminal.displayInfo("Module: "+k+" State: "+v);
                    });
                    continue;
                case "disableModule":
                    String module = formatedLine[1];
                    InfinityRegister.deactivateModule(module,bot);
                    Terminal.displayInfo("Done");
                    continue;
                case "activateModule":
                    module = formatedLine[1];
                    InfinityRegister.activateModule(module,bot);
                    Terminal.displayInfo("Done");
                    continue;
            }
        }
    }
}
