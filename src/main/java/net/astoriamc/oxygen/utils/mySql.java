package net.astoriamc.oxygen.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class mySql {


    private Connection connection = null;

    public Connection openConnection() {
        String host = Refrances.ip;
        int port = Refrances.port;
        String database = Refrances.database;
        String user = Refrances.user;
        String password = Refrances.password;


        try {

            synchronized (this) {
                if (connection != null && !connection.isClosed()) {
                    return null;
                }

                Class.forName("com.mysql.cj.jdbc.Driver");
                connection = (DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, user, password));
                return connection;
            }
        } catch (SQLException | ClassNotFoundException e) {
            Terminal.displayFatal("Could not connect to mysql");
            Terminal.displayFatal(e.toString());
            System.exit(1);
        }
        return null;
    }


    public void iniSql(Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        statement.execute("CREATE TABLE IF NOT EXISTS GlobalData(\n" +
                "    UUPH        int,\n" +
                "    UberCoins        int\n" +
                ");");
        statement.execute("CREATE TABLE IF NOT EXISTS UUPH(\n" +
                "    UUIDMC        LONGTEXT,\n" +
                "    DCID        LONGTEXT,\n" +
                "    DCNAME        LONGTEXT,\n" +
                "    RANG        LONGTEXT,\n" +
                "    UUIDMCHASH        int,\n" +
                "    UUIDDCHASH        int,\n" +
                "    UUPH        int\n" +
                ");");
        Terminal.displayInfo("mySql has been initialized");
    }
}
