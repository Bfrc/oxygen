package net.astoriamc.oxygen.utils.socket;

import club.minnced.discord.webhook.WebhookClient;
import club.minnced.discord.webhook.send.WebhookMessageBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import net.astoriamc.oxygen.Bot;
import net.astoriamc.oxygen.utils.Refrances;
import net.astoriamc.oxygen.utils.Terminal;
import net.astoriamc.oxygen.utils.async;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class Server {
    private ServerSocket serverSocket;
    private HashMap<String, String> webhooki = new HashMap<>();
    private Bot bot;

    public Server(Bot bot) {
        this.bot = bot;
    }

    public void start(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        while (true)
            new EchoClientHandler(serverSocket.accept()).start();
    }

    public void stop() throws IOException {
        serverSocket.close();
    }

    private class EchoClientHandler extends Thread {
        private final Socket clientSocket;
        private String inputLine;
        private PrintWriter out;
        private BufferedReader in;

        public EchoClientHandler(Socket socket) {
            this.clientSocket = socket;
        }


        public void run() {
            try {
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                in = new BufferedReader(
                        new InputStreamReader(clientSocket.getInputStream()));
            } catch (IOException e) {
                e.printStackTrace();
            }
            Statement statement = null;
            Connection connection = bot.getConnection();
            try {
                statement = connection.createStatement();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            while (true) {
                if (!bot.initialized) {
                    Terminal.displayError("Received socket message but bot not initialized");
                    out.println(false);
                    continue;
                }
                try {
                    if ((inputLine = in.readLine()) == null) break;
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Terminal.displayInfo("Received json: " + inputLine);
                JsonElement jsonElement = new JsonParser().parse(inputLine);
                JsonObject jsonObject = jsonElement.getAsJsonObject();
                if (inputLine.equals("ShutdownSession")) {
                    out.println(true);
                    break;
                }
                switch (jsonObject.get("type").getAsString()) {
                    case "acceptLink":
                        out.println(true);
                        String id = jsonObject.get("id").getAsString();
                        User user = bot.getUser(id);
                        System.out.println(user);
                        String nameMC = jsonObject.get("nameMc").getAsString();
                        String rank = jsonObject.get("rank").getAsString();
                        assert user != null;
                        user.openPrivateChannel().queue(channel -> { // this is a lambda expression
                            EmbedBuilder embed = new EmbedBuilder()
                                    .setImage("https://minotar.net/avatar/" + nameMC + "/100.png")
                                    .addField("Name:", nameMC, true)
                                    .addField("Linked", "Yes", true)
                                    .addField("rank:", rank, true);
                            channel.sendMessage(embed.build()).queue();
                        });
                        String sql = "UPDATE UUPH SET DCID = " + user.getId() + ", DCNAME = \"" + user.getName() + "\" WHERE UUPH = " + jsonObject.get("UUPH").getAsInt() + ";";
                        async.statementExecuteAsyncUpdate(sql, connection);
                        Guild botGuild = bot.api.getGuildById(Refrances.guild);
                        Member m = botGuild.getMemberById(user.getId());
                        m.getRoles().add(bot.api.getRoleById(Refrances.VerifiedRole));
                        m.modifyNickname("[" + rank + "] " + nameMC);
                        out.println(true);
                        continue;
                    case "msg":
                        String msg = jsonObject.get("msg").getAsString();
                        String name = jsonObject.get("user").getAsString();
                        String channel = jsonObject.get("channel").getAsString();
                        rank = jsonObject.get("rank").getAsString();
                        Guild guild = bot.api.getGuildById(Refrances.guild);
                        TextChannel textChannel = guild.getTextChannelById(channel);
                        if (textChannel == null) {
                            Terminal.displayError("Could not find channel:" + channel);
                            continue;
                        }
                        String webhhokurl;
                        if (webhooki.get(textChannel.getId()) != null) {
                            webhhokurl = webhooki.get(textChannel.getId());
                        } else {
                            Webhook web = textChannel.createWebhook("Spigot Server Message").complete();
                            webhhokurl = web.getUrl();
                            webhooki.put(textChannel.getId(), web.getUrl());
                        }
                        webhhokurl = webhhokurl.replaceAll("https://discord.com", "https://discordapp.com");
                        WebhookClient client = WebhookClient.withUrl(webhhokurl);
                        WebhookMessageBuilder builder = new WebhookMessageBuilder();
                        if (!rank.equals("default")) {
                            builder.setUsername("[" + rank + "] " + name);
                        } else {
                            builder.setUsername(name);
                        }
                        if (msg.contains("@everyone") || msg.contains("@here")) {
                            builder.setContent("Quack Quack I`m a duck that tries to ping everyone");
                        } else {
                            builder.setContent(msg.replaceAll("@", "\"@\""));
                        }
                        builder.setAvatarUrl("https://minotar.net/helm/" + name);
                        client.send(builder.build());
                        client.close();
                        continue;


                }
            }

            try {

                in.close();
                out.close();
                clientSocket.close();
            } catch (Exception e) {
                System.out.println("error");
            }
        }
    }
}
