package net.astoriamc.oxygen.utils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SqlStaticUtils {
    public static int getUUPHbyDiscordId(String id, Connection connection) throws SQLException {
        Statement statement = connection.createStatement();
        int puuph = -1;
        try {
            String sql = "SELECT * FROM UUPH where DCID = " + id + ";";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                puuph = rs.getInt("UUPH");
            }
        } catch (SQLException throwable) {
            puuph = -1;
        }
        return puuph;
    }
}
