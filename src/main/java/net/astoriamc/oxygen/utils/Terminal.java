package net.astoriamc.oxygen.utils;

public class Terminal {
    public static void displayInfo(String string) {
        print("\u001b[44;1m\u001b[37;1mINFO\u001b[0m " + string);
    }

    public static void displayWarning(String string) {
        print("\u001b[43m\u001b[37;1mWARINIG\u001b[0m " + string);
    }

    public static void displayError(String string) {
        print("\u001b[41m\u001b[37;1mERROR\u001b[0m " + string);
    }

    public static void displayFatal(String string) {
        print("\u001b[41m\u001b[41mFATAL\u001b[0m  " + string);
    }

    public static void print(String s) {
        System.out.println(s);
    }
}
