package net.astoriamc.oxygen.utils;

import java.util.HashMap;

public class Cache {
    public HashMap<String, Integer> ChatUsers = new HashMap<>();
    public HashMap<String, String> hmap = new HashMap<>();
    public HashMap<String, Class> commands = new HashMap<>();
    public HashMap<String, Boolean> ActiveCommands = new HashMap<>();
}
