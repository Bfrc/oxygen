package net.astoriamc.oxygen.infinityJda;

import net.astoriamc.oxygen.Bot;
import net.astoriamc.oxygen.utils.Terminal;

import java.util.HashMap;
import java.util.List;

public class InfinityRegister {
    public static void addHandler(String command, Object c, Bot bot) {
        Terminal.displayInfo("Registering: "+command);
        bot.getCache().commands.put(command, c.getClass());
        bot.getCache().ActiveCommands.put(command, true);
    }

    public static void addHandler(List<String> commands, Object c, Bot bot) {
        for (int i = 0; i <= commands.size()-1; i++) {
            Terminal.displayInfo("Registering: "+commands.get(i));
            bot.getCache().commands.put(commands.get(i), c.getClass());
            bot.getCache().ActiveCommands.put(commands.get(i), true);
        }
    }

    public static void removeHandler(String command, Bot bot) {
        bot.getCache().commands.remove(command);
    }
    public static void deactivateModule(String command, Bot bot){
        bot.getCache().ActiveCommands.remove(command);
        bot.getCache().ActiveCommands.put(command, false);
    }
    public static void activateModule(String command, Bot bot){
        bot.getCache().ActiveCommands.remove(command);
        bot.getCache().ActiveCommands.put(command, true);
    }
    public static HashMap<String,Boolean> getActivateModules(Bot bot){
        return bot.getCache().ActiveCommands;
    }
}
