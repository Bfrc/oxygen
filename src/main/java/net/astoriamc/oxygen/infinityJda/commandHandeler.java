package net.astoriamc.oxygen.infinityJda;

import net.astoriamc.oxygen.Bot;
import net.astoriamc.oxygen.discord.listeners.DiscordMinecraftSyncHandler;
import net.astoriamc.oxygen.utils.Terminal;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class commandHandeler extends ListenerAdapter {
    private Bot bot;

    public commandHandeler(Bot bot) {
        this.bot = bot;
    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        Message msg = event.getMessage();
        if (bot.getCache().hmap.get(event.getChannel().getId()) != null && !event.getAuthor().isBot()) {
            DiscordMinecraftSyncHandler.handle(event,bot);
        }
        ArrayList<Character> chars = new ArrayList<>();
        if (msg.getContentRaw().charAt(0) != '!'){
            return;
        }
        String commandWithoutSymbol = event.getMessage().getContentRaw().substring(1, event.getMessage().getContentRaw().length()-1);
        Terminal.displayInfo(commandWithoutSymbol);
        String pureCommand = commandWithoutSymbol.split(" ")[0];
        Terminal.displayInfo("User: "+event.getAuthor().getName()+" issued command: " + pureCommand);
        try {
            if (bot.getCache().ActiveCommands.get(pureCommand) != true) {
                Terminal.displayWarning("User: " + event.getAuthor().getName() + " tried to execute command " + pureCommand + " but command is disabled");
                return;
            }
        }catch (NullPointerException e){
            Terminal.displayError("User: " + event.getAuthor().getName() + " tried to execute command " + pureCommand + " but command in not declared");
        }
        Class clazz = bot.getCache().commands.get(pureCommand);
        if (clazz == null) {
            return;
        }
        Method method = null;
        try {
            method = clazz.getMethod("handle", MessageReceivedEvent.class, Bot.class);
            Terminal.displayInfo(method.toString());
            method.invoke(null, event, bot);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
