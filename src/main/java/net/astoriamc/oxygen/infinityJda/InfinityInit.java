package net.astoriamc.oxygen.infinityJda;

import net.astoriamc.oxygen.Bot;
import net.astoriamc.oxygen.discord.commands.ban;
import net.astoriamc.oxygen.discord.commands.help;
import net.astoriamc.oxygen.discord.commands.id;
import net.astoriamc.oxygen.discord.commands.link;

import java.util.ArrayList;


public class InfinityInit {
    public InfinityInit(Bot bot) {
        ArrayList<String> a = new ArrayList<>();
        a.add("kick");
        a.add("ban");
        a.add("tempban");
        InfinityRegister.addHandler("id", new id(), bot);
        InfinityRegister.addHandler(a, new ban(), bot);
        InfinityRegister.addHandler("link", new link(), bot);
        InfinityRegister.addHandler("help", new help(), bot);
    }
}
