package net.astoriamc.oxygen.discord.listeners;

import net.astoriamc.oxygen.Bot;
import net.astoriamc.oxygen.utils.Terminal;
import net.dv8tion.jda.api.OnlineStatus;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.Random;

public class ReadyEvent extends ListenerAdapter {
    private Bot bot;

    public ReadyEvent(Bot bot) {
        this.bot = bot;
    }

    @Override
    public void onReady(net.dv8tion.jda.api.events.ReadyEvent e) {
        Terminal.displayInfo("Bot has been initialized");
        Terminal.displayInfo("Found " + e.getGuildAvailableCount() + " available guilds");
        Terminal.displayInfo("Found " + e.getGuildUnavailableCount() + " unavailable guilds");
        Terminal.displayInfo("Found " + e.getGuildTotalCount() + " guilds");
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("PbO2");
        arrayList.add("NaBiO3");
        arrayList.add("KNO3");
        arrayList.add("N2O");
        arrayList.add("KMnO4");
        arrayList.add("O3");
        arrayList.add("O2");
        arrayList.add("H2O2");

        bot.api.setActivity(Activity.watching("Oxidizing agent: " + arrayList.get(getRandomNumberUsingInts(0, arrayList.toArray().length - 1))));
        bot.api.setStatus(OnlineStatus.ONLINE);
        bot.initialized = true;
    }

    public int getRandomNumberUsingInts(int min, int max) {
        Random random = new Random();
        return random.ints(min, max)
                .findFirst()
                .getAsInt();
    }
}
