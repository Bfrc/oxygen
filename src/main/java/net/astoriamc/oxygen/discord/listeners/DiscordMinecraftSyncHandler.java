package net.astoriamc.oxygen.discord.listeners;

import com.google.gson.JsonObject;
import net.astoriamc.oxygen.Bot;
import net.astoriamc.oxygen.utils.Refrances;
import net.astoriamc.oxygen.utils.SqlStaticUtils;
import net.astoriamc.oxygen.utils.Terminal;
import net.astoriamc.oxygen.utils.async;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.json.JSONObject;

import java.lang.ref.Reference;
import java.sql.SQLException;

public class DiscordMinecraftSyncHandler {
    public static void handle(MessageReceivedEvent event, Bot bot) {
        String server = bot.getCache().hmap.get(event.getChannel().getId());
        JSONObject jo = new JSONObject();
        jo.put("typeSpigot","DisMes");
        jo.put("typeBungee","forward");
        try {
            jo.put("UUPH", SqlStaticUtils.getUUPHbyDiscordId(event.getAuthor().getId(),bot.getConnection()));
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        jo.put("Msg",event.getMessage().getContentRaw());
        jo.put("Server",server);
        Terminal.displayInfo(jo.toString());
        async.sendMessage(jo.toString(), Refrances.SocketIp,5000);
    }
}
