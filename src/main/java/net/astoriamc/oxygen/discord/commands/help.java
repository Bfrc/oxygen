package net.astoriamc.oxygen.discord.commands;

import net.astoriamc.oxygen.Bot;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.awt.*;

public class help {
    public static void handle(MessageReceivedEvent event, Bot bot) {
        EmbedBuilder embed = new EmbedBuilder()
                .setTitle("Commands")
                .addField("!id", "shows bot version", true)
                .addField("!help", "shows commands", true)
                .addField("!link", "Links you account to mc", true)
                .setColor(Color.CYAN);
        event.getChannel().sendMessage(embed.build()).queue();
    }
}
