package net.astoriamc.oxygen.discord.commands;

import net.astoriamc.oxygen.Bot;
import net.astoriamc.oxygen.utils.Refrances;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.awt.*;

public class id {
    public static void handle(MessageReceivedEvent event, Bot bot) {
        EmbedBuilder embed = new EmbedBuilder()
                .setTitle(Refrances.id)
                .addField("Version:", "prototype", true)
                .addField("Webpage:", "http://astoriamc.net/", true)
                .addField("Author:", "Bfrc", true)
                .setColor(Color.CYAN);
        event.getChannel().sendMessage(embed.build()).queue();
    }
}
