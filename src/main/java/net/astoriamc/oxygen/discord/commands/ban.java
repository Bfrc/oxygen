package net.astoriamc.oxygen.discord.commands;

import net.astoriamc.oxygen.Bot;
import net.astoriamc.oxygen.utils.Refrances;
import net.astoriamc.oxygen.utils.async;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ban {
    public static void handle(MessageReceivedEvent event, Bot bot) {
        String[] message = event.getMessage().getContentRaw().split(" ");
        Member sender = bot.api.getGuildById(Refrances.guild).getMember(event.getAuthor());

        if (!sender.getPermissions().contains(Permission.BAN_MEMBERS)) {
            event.getChannel().sendMessage("error: You don`t have permission to do that").queue();
        }
        switch (message[0]) {
            case "!ban":
                String reason = message[2];
                Member member = event.getMessage().getMentionedMembers().get(0);
                JSONObject jo = new JSONObject();
                jo.put("typeBungee", "ban");
                String sql = "SELECT * FROM UUPH WHERE UUIDDC = " + member.getId() + " ;";
                int uuph = 0;
                try {
                    Statement statement = bot.getConnection().createStatement();
                    ResultSet result = statement.executeQuery(sql);
                    while (result.next()) {
                        uuph = result.getInt("UUPH");
                    }
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                jo.put("UUPH", uuph);
                jo.put("REASON", reason);
                async.sendMessage(jo.toString(), Refrances.SocketIp, 5000);
                member.ban(-1).queue();
                return;
            case "!tempban":
                reason = message[2];
                member = event.getMessage().getMentionedMembers().get(0);
                jo = new JSONObject();
                jo.put("typeBungee", "tempBan");
                sql = "SELECT * FROM UUPH WHERE UUIDDC = " + member.getId() + " ;";
                uuph = 0;
                int duration = Integer.parseInt(message[3]);
                try {
                    Statement statement = bot.getConnection().createStatement();
                    ResultSet result = statement.executeQuery(sql);
                    while (result.next()) {
                        uuph = result.getInt("UUPH");
                    }
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                jo.put("UUPH", uuph);
                jo.put("REASON", reason);
                jo.put("DURATION", duration);
                async.sendMessage(jo.toString(), Refrances.SocketIp, 5000);
                member.ban(duration).queue();
                return;
            case "!kick":
                reason = message[2];
                member = event.getMessage().getMentionedMembers().get(0);
                jo = new JSONObject();
                jo.put("typeBungee", "kick");
                sql = "SELECT * FROM UUPH WHERE UUIDDC = " + member.getId() + " ;";
                uuph = 0;
                try {
                    Statement statement = bot.getConnection().createStatement();
                    ResultSet result = statement.executeQuery(sql);
                    while (result.next()) {
                        uuph = result.getInt("UUPH");
                    }
                } catch (SQLException throwables) {
                    throwables.printStackTrace();
                }
                jo.put("UUPH", uuph);
                jo.put("REASON", reason);
                async.sendMessage(jo.toString(), Refrances.SocketIp, 5000);
                member.kick().queue();
                return;
        }
    }
}
