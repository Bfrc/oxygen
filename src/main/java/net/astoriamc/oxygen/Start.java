package net.astoriamc.oxygen;


import net.astoriamc.oxygen.utils.Terminal;
import org.apache.log4j.BasicConfigurator;

import javax.security.auth.login.LoginException;
import java.sql.SQLException;

public class Start {

    public static void main(String[] args) {
        BasicConfigurator.configure();
        try {
            new Bot();
        } catch (SQLException throwable) {
            Terminal.displayError(throwable.getMessage());
        }
    }
}
